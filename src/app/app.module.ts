import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './paginas/usuarios/index.component';
import { HttpClientModule } from '@angular/common/http';
import { CrearUsuarioComponent } from './paginas/usuarios/crear-usuario/crear-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditarUsuarioComponent } from './paginas/usuarios/editar-usuario/editar-usuario.component';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    CrearUsuarioComponent,
    EditarUsuarioComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
