import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  usuarios = [];
  constructor(
   public userService: UserService,
   public router: Router,
  ) { }

  ngOnInit() {
    this.cargarData();
  }

  cargarData(){
    this.userService.index().subscribe((data: any) => {
      this.usuarios = data;
    })
  }

  eliminarUsuario(id:number){
    if(confirm("Seguro desea eliminar este usuario")){   
      this.userService.delete(id).subscribe((data:any)=> {
        //this.cargarData();
        this.usuarios = this.usuarios.filter(usuario => {
          return usuario.id != data.id;
        })
      }, err => {
        console.log(err);
      })
    }
  }

}
