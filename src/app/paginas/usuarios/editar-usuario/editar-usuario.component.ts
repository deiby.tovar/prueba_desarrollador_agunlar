import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../service/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

  user: FormGroup;
  errors: any = {}
  id:number = 0
  constructor(
    public userService: UserService,
    public router: Router,
    public fb: FormBuilder,
    public arouter:ActivatedRoute,
  ) { 
    this.user = this.fb.group({
      documento:['',[Validators.required, Validators.pattern('^[0-9]+$')]],
      nombres:['',Validators.required],
      apellidos:['',Validators.required],
      email:['',[Validators.required,Validators.email]],
      telefono:['',[Validators.required,Validators.pattern('^[0-9]+$')]],
    })
  }

  ngOnInit() {
    this.arouter.params.subscribe(params => {
      this.id = params.id;
      this.userService.find( params.id).subscribe((data:any) => {
        this.user.setValue({
          documento:data.documento,
          nombres:data.nombres,
          apellidos:data.apellidos,
          email:data.email,
          telefono:data.telefono
        })
      })
    })
  }

  editarUsuario() {
    if(!this.user.invalid){
      this.userService.update(this.user.value,this.id).subscribe(data => {
        this.router.navigateByUrl('');
      },err => {
        console.log(err.error)
        this.errors = err.error
      })
    }
  }

}
