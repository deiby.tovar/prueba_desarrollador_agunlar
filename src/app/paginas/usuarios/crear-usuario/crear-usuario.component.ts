import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UserService } from '../../../service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  user: FormGroup;
  errors: any = {}
  constructor(
    public route: Router,
    public userService: UserService,
    public fb: FormBuilder
  ) { 
    this.user = this.fb.group({
      documento:['',[Validators.required, Validators.pattern('^[0-9]+$')]],
      nombres:['',Validators.required],
      apellidos:['',Validators.required],
      email:['',[Validators.required,Validators.email]],
      telefono:['',[Validators.required,Validators.pattern('^[0-9]+$')]],
    })
  }
  
  ngOnInit() {
  }

  crearUsuario(){
    console.log(this.user.invalid);
    if(!this.user.invalid){
      console.log(this.user);
      this.userService.store(this.user.value).subscribe(data => {
        this.route.navigateByUrl('');
      },err => {
        console.log(err.error)
        this.errors = err.error
      })
    }
  }

}
