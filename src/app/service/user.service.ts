import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BASE } from '../../globlas';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    public http: HttpClient
  ) { }


  index() {
    return this.http.get(`${URL_BASE}/usuarios`);
  }

  store(data: any) {
    return this.http.post(`${URL_BASE}/usuarios`, data);
  }

  find(id: number) {
    return this.http.get(`${URL_BASE}/usuarios/${id}`)
  }

  update(data: any, id: number) {
    return this.http.put(`${URL_BASE}/usuarios/${id}`, data)
  }

  delete(id: number){
    return this.http.delete(`${URL_BASE}/usuarios/${id}`)
  }
}
