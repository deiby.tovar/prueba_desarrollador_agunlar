import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './paginas/usuarios/index.component';
import { CrearUsuarioComponent } from './paginas/usuarios/crear-usuario/crear-usuario.component';
import { EditarUsuarioComponent } from './paginas/usuarios/editar-usuario/editar-usuario.component';


const routes: Routes = [
  { path: '',  component: IndexComponent },
  { path: 'usuarios/crear',  component: CrearUsuarioComponent },
  { path: 'usuarios/editar/:id',  component: EditarUsuarioComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
